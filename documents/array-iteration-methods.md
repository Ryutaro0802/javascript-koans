# 配列 反復メソッド

配列の要素に対して順に処理を行います。

ロジックへの関与が大きく、なおかつ副作用が発生しやすいので注意が必要です。

## メソッド一覧

| メソッド                      | 返り値    |
| ----------------------------- | --------- |
| Array.prototype.forEach()     | undefined |
| Array.prototype.find()        | any       |
| Array.prototype.findIndex()   | Number    |
| Array.prototype.every()       | Boolean   |
| Array.prototype.some()        | Boolean   |
| Array.prototype.filter()      | Array     |
| Array.prototype.map()         | Array     |
| Array.prototype.reduce()      | any       |
| Array.prototype.reduceRight() | any       |

## メソッドの使い分け

1.  副作用のある処理を実装したい → `forEach`
1.  配列の要素を判定したい → `every`, `some`
1.  配列の要素、インデックスを取得したい → `find`, `findIndex`
1.  配列を配列へ変形したい → `filter`, `map`, `reduce`, `reduceRight`
1.  配列を配列以外へ変形したい → `reduce`, `reduceRight`

### forEach

`forEach` は反復メソッドのなかで唯一返り値を持ちません。

そのため、副作用のある処理を実装する目的で使うとよいでしょう。

他のメソッドは返り値を取得する目的で使用するようにし、副作用のある処理の実装は避けるべきです。

悪い例：`/samples/array-filter-with-side-effect.js`

この例では `filter()` を使い、副作用を持つ処理を実行しています。

「配列をフィルタリングして新しい配列を取得する」という本来期待される使い方ではなく、単なるループの代わりに使ってしまっています。

このような、返り値が不要で単にループを実現したい場合には、`filter()` ではなく `forEach()` を使うほうが望ましいでしょう。
