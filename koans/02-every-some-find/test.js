const { shop, shopWithTokyoCustomers, tokyo, okinawa, mano } = require("./testData");

const anyCustomersAreFromTokyo = false;
const allCustomersAreFromTokyo = true;
const resultAnyCustomersAreFromTokyo = shop.checkAllCustomersAreFrom(tokyo);
const resultAllCustomersAreFromTokyo = shopWithTokyoCustomers.checkAllCustomersAreFrom(tokyo);

const isValidEvery = anyCustomersAreFromTokyo === resultAnyCustomersAreFromTokyo && allCustomersAreFromTokyo === resultAllCustomersAreFromTokyo;

const hasCustomerFromTokyo = true;
const hasCustomerFromOkinawa = false;
const resultHasCustomerFromTokyo = shop.hasCustomerFrom(tokyo);
const resultHasCustomerFromOkinawa = shop.hasCustomerFrom(okinawa);

const isValidSome = hasCustomerFromTokyo === resultHasCustomerFromTokyo && hasCustomerFromOkinawa === resultHasCustomerFromOkinawa;

const customerFromTokyo = mano;
const customerFromOkinawa = null;
const resultCustomerFromTokyo = shop.findAnyCustomerFrom(tokyo);
const resultCustomerFromOkinawa = shop.findAnyCustomerFrom(okinawa);

const isValidFind = customerFromTokyo === resultCustomerFromTokyo && customerFromOkinawa === resultCustomerFromOkinawa;

if (!isValidEvery) {
  console.log("shop.checkAllCustomersAreFrom Failed...");
}

if (!isValidSome) {
  console.log("shop.hasCustomerFrom Failed...");
}

if (!isValidFind) {
  console.log("shop.findAnyCustomerFrom Failed...");
}

if (isValidEvery && isValidSome && isValidFind) {
  console.log("Congratulations!!");
}
