const Shop = require("../classes/Shop");

class MyShop extends Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    super(name, customers);
  }

  /**
   * @param {City} city 都市
   * @returns {Boolean} 結果
   */
  checkAllCustomersAreFrom() {
    // Implement here.
  }

  /**
   * @param {City} city 都市
   * @returns {Boolean} 結果
   */
  hasCustomerFrom() {
    // Implement here.
  }

  /**
   * @param {City} city 都市名
   * @returns {Customer|null} 顧客。存在しない場合は null
   */
  findAnyCustomerFrom() {
    // Implement here.
  }
}

module.exports = MyShop;
