# All Any Count Find

`MyShop.js` の `checkAllCustomersAreFrom()` メソッドと `hasCustomerFrom()` メソッドと `findAnyCustomerFrom()` メソッドを実装してください。

`findAnyCustomerFrom()` メソッドでは、該当する顧客が複数いる場合でもいずれかひとりのみ取得できればよいこととします。

実装後、`test.js` を実行してください。

## Array.prototype.every()

例：`/samples/array-every.js`

## Array.prototype.some()

例：`/samples/array-some.js`

## Array.prototype.find()

例：`/samples/array-find.js`
