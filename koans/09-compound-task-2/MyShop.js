const Shop = require("../classes/Shop");

class MyShop extends Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    super(name, customers);
  }

  /**
   * @returns {Array<Product>} Product の配列
   */
  allOrderedProducts() {
    return this.customers.reduce((accumulator, customer) => {
      return accumulator.concat(customer.getOrderedProducts());
    }, []);
  }

  /**
   * @returns {Array<Product>} Product の配列
   */
  getProductsOrderedByAllCustomer() {
    // Implement here.
  }
}

module.exports = MyShop;
