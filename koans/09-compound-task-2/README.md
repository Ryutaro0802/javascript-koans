# Compound Task

`MyShop.js` の `getProductsOrderedByAllCustomer()` メソッドを実装してください。

`getProductsOrderedByAllCustomer()` メソッドの返り値では、要素の重複を認めることとします。

実装後、`test.js` を実行してください。
