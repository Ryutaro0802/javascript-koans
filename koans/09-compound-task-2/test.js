const { shop, shopWithNoCustomer, shopWithSoyLatteCustomers, soyLatte } = require("./testData");

const none = [];
const noProducts = [];
const soyLattes = [soyLatte, soyLatte];
const resultNone = shop.getProductsOrderedByAllCustomer();
const resultNoProducts = shopWithNoCustomer.getProductsOrderedByAllCustomer();
const resultSoyLattes = shopWithSoyLatteCustomers.getProductsOrderedByAllCustomer();

const isValidNone = JSON.stringify(none) === JSON.stringify(resultNone);
const isValidNoProducts = JSON.stringify(noProducts) === JSON.stringify(resultNoProducts);
const isValidSoyLattes = JSON.stringify(soyLattes) === JSON.stringify(resultSoyLattes);

if (!isValidNone || !isValidNoProducts || !isValidSoyLattes) {
  console.log("shop.getProductsOrderedByAllCustomer Failed...");
}

if (isValidNone && isValidNoProducts && isValidSoyLattes) {
  console.log("Congratulations!!");
}
