# GrounpBy

`MyShop.js` の `groupCustomersByCityName()` メソッドを実装してください。

実装後、`test.js` を実行してください。

## GroupBy

配列をオブジェクトに変換するような処理です。

**変更前**

```
[
  {
    date: "20180101",
    fruit: "apple"
  },
  {
    date: "20180101",
    fruit: "banana"
  },
  {
    date: "20180102",
    fruit: "orange"
  },
  {
    date: "20180103",
    fruit: "lemon"
  },
  {
    date: "20180103",
    fruit: "melon"
  }
]
```

**変更後**

```
{
  "20180101": [{ date: "20180101", fruit: "apple" }, { date: "20180101", fruit: "banana" }],
  "20180102": [{ date: "20180102", fruit: "orange" }],
  "20180103": [{ date: "20180103", fruit: "lemon" }, { date: "20180103", fruit: "melon" }]
}
```

例：`/samples/array-group-by.js`
