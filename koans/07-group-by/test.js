const { shop, aomori, fukushima, tokyo, chiba, kanagawa, aichi, toyama, kochi, tottori, yamaguchi, nagasaki, massachusetts, mano, hiori, meguru, kogane, mamimi, sakuya, yuika, kiriko, kaho, chiyoko, juri, rinze, natsuha, amana, tenka, chiyuki } = require("./testData");

const customersByCityName = {
  [tokyo.name]: [mano, hiori, kaho],
  [massachusetts.name]: [meguru],
  [nagasaki.name]: [kogane],
  [kanagawa.name]: [mamimi, juri],
  [kochi.name]: [sakuya],
  [fukushima.name]: [yuika],
  [aomori.name]: [kiriko],
  [chiba.name]: [chiyoko],
  [tottori.name]: [rinze],
  [aichi.name]: [natsuha],
  [toyama.name]: [amana, tenka],
  [yamaguchi.name]: [chiyuki]
};
const resultCustomersByCityName = shop.groupCustomersByCityName();

const isValidCustomersByCityName = JSON.stringify(customersByCityName) === JSON.stringify(resultCustomersByCityName);

if (!isValidCustomersByCityName) {
  console.log("shop.groupCustomersByCityName Failed...");
}

if (isValidCustomersByCityName) {
  console.log("Congratulations!!");
}
