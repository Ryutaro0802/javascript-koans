const Shop = require("../classes/Shop");

class MyShop extends Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    super(name, customers);
  }

  /**
   * @returns {Object<String, Array<Customer>>} key : cityName, value : Customer の配列
   */
  groupCustomersByCityName() {
    // Implement here.
  }
}

module.exports = MyShop;
