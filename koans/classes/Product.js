class Product {
  /**
   * 商品
   * @param {String} name 商品名
   * @param {Number} price 値段
   */
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
}

module.exports = Product;
