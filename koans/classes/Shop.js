class Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    this.name = name;
    this.customers = customers;
  }
}

module.exports = Shop;
