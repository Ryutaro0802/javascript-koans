class Customer {
  /**
   * 顧客
   * @param {String} name 顧客名
   * @param {City} city City インスタンス
   * @param {Array<Order>} orders Order インスタンスの一覧
   */
  constructor(name, city, orders) {
    this.name = name;
    this.city = city;
    this.orders = orders;
  }
}

module.exports = Customer;
