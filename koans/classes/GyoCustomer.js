const Customer = require("./Customer");

class GyoCustomer extends Customer {
  /**
   * 顧客
   * @param {String} name 顧客名
   * @param {City} city City インスタンス
   * @param {Array<Order>} orders Order インスタンスの一覧
   */
  constructor(name, city, orders) {
    super(name, city, orders);
  }

  /**
   * @returns {Array<Product>} Product の配列
   */
  getOrderedProducts() {
    return this.orders.reduce((accumulator, order) => {
      return accumulator.concat(order.products);
    }, []);
  }

  /**
   * @returns {Number} 金額
   */
  getTotalOrderPrice() {
    return this.getOrderedProducts().reduce((accumulator, product) => {
      return accumulator + product.price;
    }, 0);
  }

  /**
   * @returns {Product|null} 商品。存在しない場合は null
   */
  getMostExpensiveOrderedProduct() {
    return this.getOrderedProducts().reduce((accumulator, product) => {
      if (accumulator === null) {
        return product;
      }

      if (accumulator.price < product.price) {
        return product;
      }

      return accumulator;
    }, null);
  }

  /**
   * @returns {Product|null} 商品。存在しない場合は null
   */
  getMostExpensiveDeliveredProduct() {
    return this.orders
      .filter(order => {
        return order.isDelivered;
      })
      .reduce((accumulator, order) => {
        return accumulator.concat(order.products);
      }, [])
      .reduce((accumulator, product) => {
        if (accumulator === null) {
          return product;
        }

        if (accumulator.price < product.price) {
          return product;
        }

        return accumulator;
      }, null);
  }
}

module.exports = GyoCustomer;
