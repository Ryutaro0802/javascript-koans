class Order {
  /**
   * 注文
   * @param {Array<Product>} products Product インスタンスの一覧
   * @param {Boolean} isDelivered 配送済みフラグ
   */
  constructor(products, isDelivered) {
    this.products = products;
    this.isDelivered = isDelivered;
  }
}

module.exports = Order;
