class City {
  /**
   * 都市
   * @param {String} name 都市名
   */
  constructor(name) {
    this.name = name;
  }
}

module.exports = City;
