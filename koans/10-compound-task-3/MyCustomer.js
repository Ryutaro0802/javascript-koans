const Customer = require("../classes/Customer");

class MyCustomer extends Customer {
  /**
   * 顧客
   * @param {String} name 顧客名
   * @param {City} city City インスタンス
   * @param {Array<Order>} orders Order インスタンスの一覧
   */
  constructor(name, city, orders) {
    super(name, city, orders);
  }

  /**
   * @returns {Product|null} 商品。存在しない場合は null
   */
  getMostExpensiveDeliveredProduct() {
    // Implement here.
  }
}

module.exports = MyCustomer;
