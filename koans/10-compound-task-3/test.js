const { soyLatte, mano, sakuya, juri } = require("./testData");

const none = null;
const notDelivered = null;
const mostExpensiveDeliveredProduct = soyLatte;
const resultNone = mano.getMostExpensiveDeliveredProduct();
const resultNotDelivered = juri.getMostExpensiveDeliveredProduct();
const resultMostExpensiveDeliveredProduct = sakuya.getMostExpensiveDeliveredProduct();

const isValidNone = none === resultNone;
const isValidNotDelivered = notDelivered === resultNotDelivered;
const isValidMostExpensiveDeliveredProduct = JSON.stringify(mostExpensiveDeliveredProduct) === JSON.stringify(resultMostExpensiveDeliveredProduct);

if (!isValidNone || !isValidNotDelivered || !isValidMostExpensiveDeliveredProduct) {
  console.log("customer.getMostExpensiveDeliveredProduct Failed...");
}

if (isValidNone && isValidNotDelivered && isValidMostExpensiveDeliveredProduct) {
  console.log("Congratulations!!");
}
