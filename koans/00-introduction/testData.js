const City = require("../classes/City");
const MyCustomer = require("./MyCustomer");
const Order = require("../classes/Order");
const Product = require("../classes/Product");
const MyShop = require("./MyShop");

// cities
const aomori = new City("Aomori");
const fukushima = new City("Fukushima");
const tokyo = new City("Tokyo");
const chiba = new City("Chiba");
const kanagawa = new City("Kanagawa");
const aichi = new City("Aichi");
const toyama = new City("Toyama");
const kochi = new City("Kochi");
const tottori = new City("Tottori");
const yamaguchi = new City("Yamaguchi");
const nagasaki = new City("Nagasaki");
const okinawa = new City("Okinawa");
const massachusetts = new City("Massachusetts");

// products
const brendCoffee = new Product("Brend coffee", 220);
const coffeeLatte = new Product("Coffee latte", 250);
const americanCoffee = new Product("American Coffee", 220);
const honeyCafeAuLait = new Product("Honey cafe au lait", 320);
const soyLatte = new Product("Soy latte", 310);
const coffeeMocha = new Product("Coffee mocha", 350);
const cappuccino = new Product("Cappuccino", 260);
const espressoCoffee = new Product("Espresso coffee", 220);
const cocoa = new Product("Cocoa", 320);

// customers
const mano = new MyCustomer("Mano", tokyo, []);
const hiori = new MyCustomer("Hiori", tokyo, []);
const meguru = new MyCustomer("Meguru", massachusetts, []);
const kogane = new MyCustomer("Kogane", nagasaki, [new Order(brendCoffee, true)]);
const mamimi = new MyCustomer("Mamimi", kanagawa, [new Order(coffeeLatte, false), new Order(americanCoffee, true)]);
const sakuya = new MyCustomer("Sakuya", kochi, [new Order(honeyCafeAuLait, false), new Order(soyLatte, true), new Order(coffeeMocha, false)]);
const yuika = new MyCustomer("Yuika", fukushima, [new Order(cappuccino, false)]);
const kiriko = new MyCustomer("Kiriko", aomori, [new Order(espressoCoffee, true), new Order(brendCoffee, false)]);
const kaho = new MyCustomer("Kaho", tokyo, [new Order(coffeeLatte, true), new Order(americanCoffee, false), new Order(honeyCafeAuLait, true)]);
const chiyoko = new MyCustomer("Chiyoko", chiba, [new Order(soyLatte, true)]);
const juri = new MyCustomer("Juri", kanagawa, [new Order(coffeeMocha, false), new Order(cappuccino, false)]);
const rinze = new MyCustomer("Rinze", tottori, [new Order(espressoCoffee, true), new Order(brendCoffee, true), new Order(coffeeLatte, true)]);
const natsuha = new MyCustomer("Natsuha", aichi, [new Order(americanCoffee, false)]);
const amana = new MyCustomer("Amana", toyama, [new Order(soyLatte, true), new Order(soyLatte, false), new Order(brendCoffee, true)]);
const tenka = new MyCustomer("Tenka", toyama, [new Order(soyLatte, true), new Order(soyLatte, false), new Order(coffeeLatte, true)]);
const chiyuki = new MyCustomer("Chiyuki", yamaguchi, [new Order(soyLatte, true), new Order(soyLatte, false), new Order(americanCoffee, true)]);

// shop
const shop = new MyShop("ドトールコーヒー 東京店", [mano, hiori, meguru, kogane, mamimi, sakuya, yuika, kiriko, kaho, chiyoko, juri, rinze, natsuha, amana, tenka, chiyuki]);
const shopWithTokyoCustomers = new MyShop("ドトールコーヒー 東京駅店", [mano, hiori, kaho]);
const shopWithNoCustomer = new MyShop("ドトールコーヒー 沖縄店", []);
const shopWithOneCustomer = new MyShop("ドトールコーヒー 長崎店", [kogane]);
const shopWithSoyLatteCustomers = new MyShop("ドトールコーヒー 富山店", [amana, tenka, chiyuki]);

module.exports = {
  shop,
  shopWithTokyoCustomers,
  shopWithNoCustomer,
  shopWithOneCustomer,
  shopWithSoyLatteCustomers,
  aomori,
  fukushima,
  tokyo,
  chiba,
  kanagawa,
  aichi,
  toyama,
  kochi,
  tottori,
  yamaguchi,
  nagasaki,
  okinawa,
  massachusetts,
  brendCoffee,
  coffeeLatte,
  americanCoffee,
  honeyCafeAuLait,
  soyLatte,
  coffeeMocha,
  cappuccino,
  espressoCoffee,
  cocoa,
  mano,
  hiori,
  meguru,
  kogane,
  mamimi,
  sakuya,
  yuika,
  kiriko,
  kaho,
  chiyoko,
  juri,
  rinze,
  natsuha,
  amana,
  tenka,
  chiyuki
};
