const Shop = require("../classes/Shop");

class MyShop extends Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    super(name, customers);
  }

  /**
   * @param {City} city 都市
   * @returns {Number} 顧客数
   */
  countCustomersFrom() {
    // Implement here.
  }
}

module.exports = MyShop;
