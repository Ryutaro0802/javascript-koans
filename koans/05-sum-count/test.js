const { shop, shopWithNoCustomer, tokyo, okinawa, mano, sakuya } = require("./testData");

const manoTotalOrderPrice = 0;
const sakuyaTotalOrderPrice = 980;
const resultManoTotalOrderPrice = mano.getTotalOrderPrice();
const resultSakuyaTotalOrderPrice = sakuya.getTotalOrderPrice();

const isValidZero = manoTotalOrderPrice === resultManoTotalOrderPrice;
const isValidTotalPrice = sakuyaTotalOrderPrice === resultSakuyaTotalOrderPrice;

const customersCountFromTokyo = 3;
const customersCountFromOkinawa = 0;
const noCustomersCount = 0;
const resultCustomersCountFromTokyo = shop.countCustomersFrom(tokyo);
const resultCustomersCountFromOkinawa = shop.countCustomersFrom(okinawa);
const resultNoCustomersCount = shopWithNoCustomer.countCustomersFrom(tokyo);

const isValidCustomersCountFromTokyo = customersCountFromTokyo === resultCustomersCountFromTokyo;
const isValidCustomersCountFromOkinawa = customersCountFromOkinawa === resultCustomersCountFromOkinawa;
const isValidNoCustomersCount = noCustomersCount === resultNoCustomersCount;

if (!isValidZero || !isValidTotalPrice) {
  console.log("customer.getTotalOrderPrice Failed...");
}

if (!isValidCustomersCountFromTokyo || !isValidCustomersCountFromOkinawa || !isValidNoCustomersCount) {
  console.log("shop.countCustomersFrom Failed...");
}

if (isValidZero && isValidTotalPrice && isValidCustomersCountFromTokyo && isValidCustomersCountFromOkinawa && isValidNoCustomersCount) {
  console.log("Congratulations!!");
}
