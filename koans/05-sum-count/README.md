# Sum Count

`MyShop.js` の `countCustomersFrom()` メソッドを実装してください。

`MyCustomer.js` の `getTotalOrderPrice()` メソッドを実装してください。

実装後、`test.js` を実行してください。

## Sum

JavaScript には `sum()` が用意されていないので、ビルドインメソッドを組み合わせて実現する必要があります。

例：`/samples/array-sum.js`

## Count

JavaScript には `count()` が用意されていないので、ビルドインメソッドを組み合わせて実現する必要があります。

例：`/samples/array-count.js`
