# JavaScript Koans

Kotlin Koans のパクりです。

https://try.kotlinlang.org/#/Kotlin%20Koans/Collections/Introduction/Task.kt

Collections セクションが楽しかったので、JavaScript の Array 操作の練習問題を作ってみました。

## 進め方

1.  `/koans/00-introduction/README.md` を読んでください。
1.  `/koans/00-introduction/MyShop.js` をエディタで開き、JSDoc とメソッド名をヒントに `helloWorld()` を実装してください。
1.  `/koans/00-introduction/test.js` を実行してみてください。例：`node koans/00-introduction/test.js`
1.  「`Congratulations!!`」と表示された場合、おめでとうございます、正しく実装できています。「`shop.helloWorld Failed...`」と表示された場合、何かが間違っています。
1.  正しい実装例は `/koans/classes/GyoShop.js` にあります。
1.  他のレッスンも同様に進めてください。
