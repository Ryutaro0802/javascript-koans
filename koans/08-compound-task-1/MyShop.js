const Shop = require("../classes/Shop");

class MyShop extends Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    super(name, customers);
  }

  /**
   * @returns {Array<Product>} Product の配列
   */
  allOrderedProducts() {
    return this.customers.reduce((accumulator, customer) => {
      return accumulator.concat(customer.getOrderedProducts());
    }, []);
  }

  /**
   * @param {Product} product 商品
   * @returns {Number} 注文数
   */
  getNumberOfTimesProductWasOrdered() {
    // Implement here.
  }
}

module.exports = MyShop;
