const { shop, soyLatte, cocoa } = require("./testData");

const cocoaCount = 0;
const soyLatteCount = 8;
const resultCocoaCount = shop.getNumberOfTimesProductWasOrdered(cocoa);
const resultSoyLatteCount = shop.getNumberOfTimesProductWasOrdered(soyLatte);

const isValidCocoaCount = cocoaCount === resultCocoaCount;
const isValidSoyLatteCount = soyLatteCount === resultSoyLatteCount;

if (!isValidCocoaCount || !isValidSoyLatteCount) {
  console.log("shop.getNumberOfTimesProductWasOrdered Failed...");
}

if (isValidCocoaCount && isValidSoyLatteCount) {
  console.log("Congratulations!!");
}
