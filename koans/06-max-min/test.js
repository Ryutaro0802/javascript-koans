const { shop, shopWithNoCustomer, soyLatte, coffeeMocha, mano, sakuya, amana } = require("./testData");

const customerWithMaximumNumberOfOrders = sakuya;
const noCustomerWithMaximumNumberOfOrders = null;
const resultCustomerWithMaximumNumberOfOrders = shop.getCustomerWithMaximumNumberOfOrders();
const resultNoCustomerWithMaximumNumberOfOrders = shopWithNoCustomer.getCustomerWithMaximumNumberOfOrders();

const isValidCustomerWithMaximumNumberOfOrders = JSON.stringify(customerWithMaximumNumberOfOrders) === JSON.stringify(resultCustomerWithMaximumNumberOfOrders);
const isValidNoCustomerWithMaximumNumberOfOrders = noCustomerWithMaximumNumberOfOrders === resultNoCustomerWithMaximumNumberOfOrders;

const noProduct = null;
const mostExpensiveOrderedProduct = coffeeMocha;
const mostExpensiveOrderedProducts = soyLatte;
const resultNoProduct = mano.getMostExpensiveOrderedProduct();
const resultMostExpensiveOrderedProduct = sakuya.getMostExpensiveOrderedProduct();
const resultMostExpensiveOrderedProducts = amana.getMostExpensiveOrderedProduct();

const isValidNoProduct = noProduct === resultNoProduct;
const isValidMostExpensiveOrderedProduct = JSON.stringify(mostExpensiveOrderedProduct) === JSON.stringify(resultMostExpensiveOrderedProduct);
const isValidMostExpensiveOrderedProducts = JSON.stringify(mostExpensiveOrderedProducts) === JSON.stringify(resultMostExpensiveOrderedProducts);

if (!isValidCustomerWithMaximumNumberOfOrders || !isValidNoCustomerWithMaximumNumberOfOrders) {
  console.log("shop.getCustomerWithMaximumNumberOfOrders Failed...");
}

if (!isValidNoProduct || !isValidMostExpensiveOrderedProduct || !isValidMostExpensiveOrderedProducts) {
  console.log("customer.getMostExpensiveOrderedProduct Failed...");
}

if (isValidCustomerWithMaximumNumberOfOrders && isValidNoCustomerWithMaximumNumberOfOrders && isValidNoProduct && isValidMostExpensiveOrderedProduct && isValidMostExpensiveOrderedProducts) {
  console.log("Congratulations!!");
}
