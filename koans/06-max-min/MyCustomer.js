const Customer = require("../classes/Customer");

class MyCustomer extends Customer {
  /**
   * 顧客
   * @param {String} name 顧客名
   * @param {City} city City インスタンス
   * @param {Array<Order>} orders Order インスタンスの一覧
   */
  constructor(name, city, orders) {
    super(name, city, orders);
  }

  /**
   * @returns {Array<Product>} Product の配列
   */
  getOrderedProducts() {
    return this.orders.reduce((accumulator, order) => {
      return accumulator.concat(order.products);
    }, []);
  }

  /**
   * @returns {Product|null} 商品。存在しない場合は null
   */
  getMostExpensiveOrderedProduct() {
    // Implement here.
  }
}

module.exports = MyCustomer;
