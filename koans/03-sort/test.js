const { shop, mano, hiori, meguru, kogane, mamimi, sakuya, yuika, kiriko, kaho, chiyoko, juri, rinze, natsuha, amana, tenka, chiyuki } = require("./testData");

const customers = [mano, hiori, meguru, kogane, mamimi, sakuya, yuika, kiriko, kaho, chiyoko, juri, rinze, natsuha, amana, tenka, chiyuki];
const customersSortedByNumberOfOrders = [kaho, chiyuki, tenka, amana, rinze, sakuya, kiriko, juri, mamimi, yuika, chiyoko, natsuha, kogane, hiori, meguru, mano];
const resultCustomersSortedByNumberOfOrders = shop.getCustomersSortedByNumberOfOrders();

const isValidOrigin = JSON.stringify(customers) === JSON.stringify(shop.customers);
const isValidSorted = JSON.stringify(customersSortedByNumberOfOrders) === JSON.stringify(resultCustomersSortedByNumberOfOrders);

if (!isValidOrigin || !isValidSorted) {
  console.log("shop.getCustomersSortedByNumberOfOrders Failed...");
}

if (isValidOrigin && isValidSorted) {
  console.log("Congratulations!!");
}
