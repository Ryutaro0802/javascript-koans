# Sort

`MyShop.js` の `getCustomersSortedByNumberOfOrders()` メソッドを実装してください。

`getCustomersSortedByNumberOfOrders()` メソッドでは、もとの `this.customers` を変更しないようにしてください。

実装後、`test.js` を実行してください。

## Array.prototype.sort()

例：`/samples/array-sort.js`
