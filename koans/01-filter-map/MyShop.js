const Shop = require("../classes/Shop");

class MyShop extends Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    super(name, customers);
  }

  /**
   * @returns {Array<City>} City の配列
   */
  getCitiesCustomersAreFrom() {
    // Implement here.
  }

  /**
   * @param {City} city 都市
   * @returns {Array<Customer>} Customer の配列
   */
  getCustomersFrom() {
    // Implement here.
  }

  /**
   * @returns {Array<Customer>} Customer の配列
   */
  getCustomersWithMoreUndeliveredOrdersThanDelivered() {
    // Implement here.
  }
}

module.exports = MyShop;
