# Filter Map

`MyShop.js` の `getCitiesCustomersAreFrom()` メソッドと `getCustomersFrom()` メソッドと `getCustomersWithMoreUndeliveredOrdersThanDelivered()` メソッドを実装してください。

実装後、`test.js` を実行してください。

## Array.prototype.filter()

例：`/samples/array-filter.js`

## Array.prototype.map()

例：`/samples/array-map.js`
