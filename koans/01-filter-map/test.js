const { shop, aomori, fukushima, tokyo, chiba, kanagawa, aichi, toyama, kochi, tottori, yamaguchi, nagasaki, okinawa, massachusetts, mano, hiori, kogane, sakuya, yuika, kaho, juri, natsuha } = require("./testData");

const cities = [tokyo, tokyo, massachusetts, nagasaki, kanagawa, kochi, fukushima, aomori, tokyo, chiba, kanagawa, tottori, aichi, toyama, toyama, yamaguchi];
const resultCities = shop.getCitiesCustomersAreFrom();

const isValidFilter = JSON.stringify(cities) === JSON.stringify(resultCities);

const customersFromTokyo = [mano, hiori, kaho];
const customersFromNagasaki = [kogane];
const customersFromOkinawa = [];
const resultCustomersFromTokyo = shop.getCustomersFrom(tokyo);
const resultCustomersFromNagasaki = shop.getCustomersFrom(nagasaki);
const resultCustomersFromOkinawa = shop.getCustomersFrom(okinawa);

const isValidMap = JSON.stringify(customersFromTokyo) === JSON.stringify(resultCustomersFromTokyo) && JSON.stringify(customersFromNagasaki) === JSON.stringify(resultCustomersFromNagasaki) && JSON.stringify(customersFromOkinawa) === JSON.stringify(resultCustomersFromOkinawa);

const customersWithMoreUndeliveredOrdersThanDelivered = [sakuya, yuika, juri, natsuha];
const resultCustomersWithMoreUndeliveredOrdersThanDelivered = shop.getCustomersWithMoreUndeliveredOrdersThanDelivered();

const isValidPartition = JSON.stringify(customersWithMoreUndeliveredOrdersThanDelivered) === JSON.stringify(resultCustomersWithMoreUndeliveredOrdersThanDelivered);

if (!isValidFilter) {
  console.log("shop.getCitiesCustomersAreFrom Failed...");
}

if (!isValidMap) {
  console.log("shop.getCustomersFrom Failed...");
}

if (!isValidPartition) {
  console.log("shop.getCustomersWithMoreUndeliveredOrdersThanDelivered Failed...");
}

if (isValidFilter && isValidMap && isValidPartition) {
  console.log("Congratulations!!");
}
