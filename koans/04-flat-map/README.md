# FlatMap

`MyCustomer.js` の `getOrderedProducts()` メソッドを実装してください。

`getOrderedProducts()` メソッドの返り値では、要素の重複を認めることとします。

`MyShop.js` の `allOrderedProducts()` メソッドを実装してください。

`allOrderedProducts()` メソッドの返り値では、要素の重複を認めることとします。

実装後、`test.js` を実行してください。

## FlatMap

多次元配列の二次元目の要素を一次元の配列に連結するような処理です。

**変換前の配列**

```
[["1-1", "1-2"], ["2-1"], ["3-1", "3-2", "3-3"]]
```

**変換後の配列**

```
["1-1", "1-2", "2-1", "3-1", "3-2", "3-3"]
```

JavaScript には `flatMap()` が用意されていないので、ビルドインメソッドを組み合わせて実現する必要があります。

以下の例では、`reduce()` と `concat()` の組み合わせで実現しています。

例：`/samples/array-flat-map.js`

`reduce()` の例：`/samples/array-reduce.js`
