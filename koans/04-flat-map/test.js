const { shop, shopWithNoCustomer, brendCoffee, coffeeLatte, americanCoffee, honeyCafeAuLait, soyLatte, coffeeMocha, cappuccino, espressoCoffee, mano, sakuya } = require("./testData");

const manoOrderedProducts = [];
const sakuyaOrderedProducts = [honeyCafeAuLait, soyLatte, coffeeMocha];
const resultManoOrderedProducts = mano.getOrderedProducts();
const resultSakuyaOrderedProducts = sakuya.getOrderedProducts();

const isValidEmptyProducts = JSON.stringify(manoOrderedProducts) === JSON.stringify(resultManoOrderedProducts);
const isValidProducts = JSON.stringify(sakuyaOrderedProducts) === JSON.stringify(resultSakuyaOrderedProducts);

const allOrderedProducts = [brendCoffee, coffeeLatte, americanCoffee, honeyCafeAuLait, soyLatte, coffeeMocha, cappuccino, espressoCoffee, brendCoffee, coffeeLatte, americanCoffee, honeyCafeAuLait, soyLatte, coffeeMocha, cappuccino, espressoCoffee, brendCoffee, coffeeLatte, americanCoffee, soyLatte, soyLatte, brendCoffee, soyLatte, soyLatte, coffeeLatte, soyLatte, soyLatte, americanCoffee];
const noOrderedProducts = [];
const resultAllOrderedProducts = shop.allOrderedProducts();
const resultNoOrderedProducts = shopWithNoCustomer.allOrderedProducts();

const isValidAllProducts = JSON.stringify(allOrderedProducts) === JSON.stringify(resultAllOrderedProducts);
const isValidNoProducts = JSON.stringify(noOrderedProducts) === JSON.stringify(resultNoOrderedProducts);

if (!isValidEmptyProducts || !isValidProducts) {
  console.log("customer.getOrderedProducts Failed...");
}

if (!isValidAllProducts || !isValidNoProducts) {
  console.log("shop.allOrderedProducts Failed...");
}

if (isValidAllProducts && isValidNoProducts && isValidEmptyProducts && isValidProducts) {
  console.log("Congratulations!!");
}
