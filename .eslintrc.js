module.exports = {
  extends: ["eslint:recommended"],
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module"
  },
  rules: {
    "no-console": 0
  }
};
