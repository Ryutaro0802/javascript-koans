const total = [1, 2, 3, 4, 5, 6].reduce(accumulator => {
  return accumulator + 1;
}, 0);

console.log(total); // 6
