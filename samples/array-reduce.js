const total = [1, 2, 3, 4, 5, 6].reduce((accumulator, item) => {
  return accumulator + item;
});

console.log(total); // 21

const text = ["Hello", "World"].reduce((accumulator, item) => {
  return accumulator + item;
});

console.log(text); // "HelloWorld"

const empty = [].reduce((accumulator, item) => {
  return accumulator + item;
}, null);

console.log(empty); // null
