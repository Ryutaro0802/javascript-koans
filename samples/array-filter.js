const filtered = [1, 2, 3, 4, 5, 6].filter(item => {
  return item < 3;
});

console.log(filtered); // [ 1, 2 ]
