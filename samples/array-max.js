const total = [1, 6, 2, 5, 3, 4].reduce((accumulator, item) => {
  if (accumulator === null) {
    return item;
  }

  if (accumulator < item) {
    return item;
  }

  return accumulator;
}, null);

console.log(total); // 6
