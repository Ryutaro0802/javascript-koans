const mapped = [1, 2, 3, 4, 5, 6].map(item => {
  return item * 2;
});

console.log(mapped); // [ 2, 4, 6, 8, 10, 12 ]
