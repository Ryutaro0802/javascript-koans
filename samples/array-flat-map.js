const origin = [["1-1", "1-2"], ["2-1"], ["3-1", "3-2", "3-3"]];

const allChildren = origin.reduce((accumulator, child) => {
  return accumulator.concat(child);
}, []);

console.log(allChildren); // ["1-1", "1-2", "2-1", "3-1", "3-2", "3-3"]
