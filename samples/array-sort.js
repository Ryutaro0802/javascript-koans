const origin = [1, 3, 5, 2, 4, 6];

const sorted = origin.sort((prev, next) => {
  return prev - next;
});

console.log(sorted); // [ 1, 2, 3, 4, 5, 6 ]
console.log(origin); // [ 1, 2, 3, 4, 5, 6 ] sort() は破壊的なメソッドなので、もとの配列も変更されてしまいます。
