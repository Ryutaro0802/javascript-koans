const grouped = [
  {
    date: "20180101",
    fruit: "apple"
  },
  {
    date: "20180101",
    fruit: "banana"
  },
  {
    date: "20180102",
    fruit: "orange"
  },
  {
    date: "20180103",
    fruit: "lemon"
  },
  {
    date: "20180103",
    fruit: "melon"
  }
].reduce((accumulator, item) => {
  if (accumulator[item.date] === undefined) {
    accumulator[item.date] = [];
  }
  accumulator[item.date].push(item);
  return accumulator;
}, {});

console.log(grouped);
// {
//   "20180101": [{ date: "20180101", fruit: "apple" }, { date: "20180101", fruit: "banana" }],
//   "20180102": [{ date: "20180102", fruit: "orange" }],
//   "20180103": [{ date: "20180103", fruit: "lemon" }, { date: "20180103", fruit: "melon" }]
// }
