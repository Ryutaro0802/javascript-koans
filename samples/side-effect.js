const doSomething = array => {
  // Do something
  array.pop(); // 引数 array の破壊的メソッド pop を使用
  // Do something
};

const names = ["Alice", "Bob", "Carol"];

console.log("names : ", names); // ["Alice", "Bob", "Carol"]

doSomething(names);

console.log("names : ", names); // ["Alice", "Bob"]
